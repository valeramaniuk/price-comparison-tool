# Zippo Lighters Price Reporting tool #

* Python/Django project to visualize information about prices for Zippo lighters in Belarus.
* Graphs are produced by Plotly http://www.zippo-price.by/graphs
* The data is being retrieved by the separate project https://bitbucket.org/valeramaniuk/zippo_scrapping.
* The results can be seen at http://www.zippo-price.by

### Contact ###
Valera MANIUK valery.maniuk.55@my.csun.edu