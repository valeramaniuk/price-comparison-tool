from collections import OrderedDict

from ..models import Stock, Store

def generate_context(datestamp):
    # context is a name of a dictionary
    # it contains specially formatted intersection of
    # for a given day
    # so it can be used to work with historical data

    # for debug during an active update
    # datestamp = '2017-03-20'


    # A list of all stores we have in the DB
    list_of_stores = Store.get_all_stores()

    # All lighters for Zippo.BY for this day
    zippoby_models = Stock.objects.filter(store='zippoby',
                                          datestamp=datestamp)
    # List of Model numbers for Zippo.BY for this day
    zippoby_model_numbers = zippoby_models.values_list('zippo_model',
                                                       flat=True)

    # All lighters for Padarunki.by
    padarunki_models = Stock.objects.filter(store='padarunki',
                                            datestamp=datestamp)
    # List of model numbers for Padarunki for this day
    padarunki_models_numbers = padarunki_models.values_list('zippo_model',
                                                            flat=True)


    # All lighters for atribut.by
    atributby_models = Stock.objects.filter(store='atributby',
                                            datestamp=datestamp)
    # List of model numbers for atribut.by for this day
    atributby_models_numbers = padarunki_models.values_list('zippo_model',
                                                            flat=True)


    # Set of model numbers for ALL stores except KW for this day
    other_stores_model_numbers_union = set(list(zippoby_model_numbers) +
                                           list(padarunki_models_numbers) +
                                           list(atributby_models_numbers)
                                           )

    # All Lighters for KW.by for this day
    # But only if they are in the list of model names for
    # all other stores for this day
    # So it's an intersection of inventories
    kw_models = Stock.objects\
        .filter(store='knifeworksby',
                datestamp=datestamp,
                zippo_model__in=other_stores_model_numbers_union)\
        .exclude(price=0.0)\
        .order_by("-price")

    # List of model numbers
    # Intersection of KW_model numbers and ALL other stores
    only_model_numbers_to_display = kw_models.values_list('zippo_model', flat=True)

    context = OrderedDict()

    for kw in kw_models:
        # If we have the picture on S3 - use it
        # If not - fetch directly from the website where
        # we found this lighter originally
        if kw.zippo_model.s3_pic_url != '':
            pic_url = kw.zippo_model.s3_pic_url
        else:
            pic_url = kw.zippo_model.pic_url
        context[kw.zippo_model.model] = {
            'knifeworksby_price': kw.price,
            'pic_url': pic_url,
            'description': kw.zippo_model.description,
            'kw_lighter_url': kw.model_url,
        }

    #TODO: refactor in a general function just like with price_delta
    for zp in zippoby_models:
        if zp.zippo_model.model in only_model_numbers_to_display \
                and zp.zippo_model.model in context:
            was = context[zp.zippo_model.model]
            to_add = {'zippoby_price': zp.price,
                      'zippoby_url': zp.model_url}
            to_add.update(was)
            context[zp.zippo_model.model] = to_add

    for pd in padarunki_models:
        if pd.zippo_model.model in only_model_numbers_to_display \
                and pd.zippo_model.model in context:
            was = context[pd.zippo_model.model]
            to_add = {'padarunki_price': pd.price,
                      'padarunki_url': pd.model_url}
            to_add.update(was)
            context[pd.zippo_model.model] = to_add

    for at in atributby_models:
        if at.zippo_model.model in only_model_numbers_to_display \
            and at.zippo_model.model in context:

            was = context[at.zippo_model.model]
            to_add = {'atributby_price': at.price,
                      'atributby_url': at.model_url}
            to_add.update(was)
            context[at.zippo_model.model] = to_add

    return context