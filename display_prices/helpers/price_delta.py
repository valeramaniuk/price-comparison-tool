
def add_price_statistics(context, store_list):

        # after all of the context dictionary info
        # is received from DB and formatted
        # we can calculate and add statistics to it
        #
        # context is a dictionary:
        # {'model':
        #   {'kw_price': 50, 'zippo_price: 60,}
        # }

        store_price_str_list = [store_name + '_price' for store_name in store_list
                                if store_name !='knifeworksby']

        for model, values in context.items():
            # values is a dictionary type
            #

            if 'knifeworksby_price' in values and values['knifeworksby_price'] != 0:
                # Because it's lazy it shouldn't get a KeyError if kw_price
                # is not in values.

                # for the future
                # {0:+.} always displays the sign of a float
                # so don't need to worry about it in the template
                kw_price = values['knifeworksby_price']

                for store_price_str in store_price_str_list:

                    if store_price_str in values:
                        store_price = values[store_price_str]
                        store_price_absolute_diff = ("{0:+.1f}".format(store_price - kw_price))
                        store_price_percentage_diff = 100*(store_price - kw_price)/kw_price
                        store_price_percentage_diff = ("{0:+.1f}".format(store_price_percentage_diff))

                        absolute_key_name = store_price_str+"_absolute_difference"
                        percentage_key_name = store_price_str+"_percentage_difference"
                        context[model].update({absolute_key_name: store_price_absolute_diff})
                        context[model].update({percentage_key_name: store_price_percentage_diff})


        return context

