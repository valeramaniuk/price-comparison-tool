from .log_helper import get_logger
from ..models import Store

logger = get_logger()


def get_total_count_object():
    # returns a dictionary {'store': total number of lighters in stock}
    total_inventory = dict()
    list_of_all_stores = Store.get_all_stores()

    for store in list_of_all_stores:
        total_inventory[store] = Store.total_lighter_this_store(store)

    return total_inventory


def relative_prices(context, total_inventory_obj):
    # updates the total_inventory_obj
    # adds the number of lighters that are cheaper than
    # on KW.by
    #
    # also adds the average price delta in % for every store
    # compare to KW.by
    # more expensive and cheaper lighters are accounted for separately
    #

    # all existing stores, including KW
    # need to watch out for KW
    all_stores = Store.get_all_stores()
    all_other_stores = Store.get_all_other_stores()

    # will hold the data on by how much on average lighters on the
    # particular store is cheaper (for those that ARE cheaper)
    #
    # same but for more expensive ones

    cheaper_average_percentage = dict()
    more_expensive_average_percentage = dict()

    #  set default values for the numbers
    # of cheaper/ more expensive lighters
    for store in all_stores:
        # will hold the number of cheaper than KWby lighters
        # for every store
        total_inventory_obj['cheaper_lighters_' + store] = 0

        # will hold the number of more expensive than KWby lighters
        # for every store
        # pitfall: do not forget that the number of more expensive ones !=
        # total number in stock minus number of cheaper ones, because we track
        # only the intersection of inventories
        total_inventory_obj['more_expensive_lighters_' + store] = 0

        # how much cheaper in average (in %) lighters in other stores
        # compare to KW
        cheaper_average_percentage['avg_cheaper_percent_' + store] = 0

        # how much more expensive in average (in %) lighters in other stores
        # compare to KW
        more_expensive_average_percentage['avg_more_expensive_percent_' + store] = 0

    for k, v in context.items():
        # v itself is a dict
        kw_price = v['knifeworksby_price']
        for store in all_other_stores:
            if store + '_price' in v:
                if kw_price > v[store + '_price']:
                    total_inventory_obj['cheaper_lighters_' + store] += 1
                else:
                    total_inventory_obj['more_expensive_lighters_' + store] += 1
                    # print(total_inventory_obj['more_expensive_lighters_' + store])

            # sum all price difference percentages for a certain store
            # then will divide by the number of cheaper/ more expensive lighters
            # respectively, to get average difference
            if store + '_price_percentage_difference' in v:
                if float(v[store + '_price_percentage_difference']) > 0:
                    more_expensive_average_percentage['avg_more_expensive_percent_' + store] \
                        += float(v[store + '_price_percentage_difference'])

                else:
                    cheaper_average_percentage['avg_cheaper_percent_' + store] \
                        += float(v[store + '_price_percentage_difference'])

    # at this point cheaper_average_percentage[] / more_expensive_average_percentage[]
    # contain the SUM of differences, we need to divide by the number of lighters
    for store in all_other_stores:
        # total number of lighters in stock for this store
        total_inventory_store = Store.total_lighter_this_store(store)
        cheaper_ones = total_inventory_obj['cheaper_lighters_' + store]

        if cheaper_ones:
            try:
                cheaper_average_percentage['avg_cheaper_percent_' + store] \
                    /= total_inventory_obj['cheaper_lighters_' + store]
            except ZeroDivisionError:
                logger.error(
                    'Relative prices calculator got the number of cheaper lighters'
                    ' ({}) as zero, and tried to divide into'
                    .format(store)
                )
        if total_inventory_obj['more_expensive_lighters_' + store] ==0:
            more_expensive_average_percentage['avg_more_expensive_percent_' + store] =0
        else:
            try:
                more_expensive_average_percentage['avg_more_expensive_percent_' + store] \
                    /= total_inventory_obj['more_expensive_lighters_' + store]
            except ZeroDivisionError:
                logger.error(
                    'Relative prices calculator got the number of more expensive lighters'
                    ' ({}) as zero, and tried to divide into'
                    .format(store)
                )
    total_inventory_obj.update(more_expensive_average_percentage)
    total_inventory_obj.update(cheaper_average_percentage)

    return total_inventory_obj
