import os
import logging

def get_logger():
    dir_path = os.path.dirname(os.path.realpath(__file__))

    logger = logging.getLogger(__name__)
    logger.setLevel(logging.INFO)

    formatter = logging.Formatter('%(asctime)s:%(name):%(message)s')
    file_handler = logging.FileHandler(os.path.join(dir_path,  'helpers_views.log'))

    file_handler.setFormatter(formatter)
    # logging_streamer = logging.StreamHandler()
    logger.addHandler(file_handler)
    # logger.addHandler(logging_streamer)
    return logger