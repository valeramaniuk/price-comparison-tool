import boto3


dynamodb = boto3.resource('dynamodb', region_name='us-west-2')
configuration_table = dynamodb.Table('configuration')

def get_plotly_credentials():

    response = configuration_table.get_item(
        Key={
            'keys': 'plotly_signin'
        }
    )
    try:
        if response['Item']['values']:
            print('Getting credentials for plot.ly SUCCESSFUL')
            return response['Item']['values']
        else:
            print('Getting credentials for plot.ly FAILED')
            return False
    except KeyError:
        print('Getting credentials for plot.ly FAILED')
        return False
