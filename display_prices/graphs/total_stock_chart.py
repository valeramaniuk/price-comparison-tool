import plotly.plotly as py
import plotly.graph_objs as go
from .dynamo_conf import get_plotly_credentials
from django.http import HttpResponse

from ..models import Stock


def get_total_stock_chart(context):
    CONF = get_plotly_credentials()
    py.sign_in(username=CONF['username'], api_key=CONF['api_key'])
    zippoby_models = []
    knifeworks_price_at_zippoby = []
    zippoby_price = []

    padarunki_models = []
    knifeworks_price_at_padarunki = []
    padarunki_price = []

    for model, model_data in context.items():
        if 'zippoby_price' in  model_data:
            zippoby_models.append('z'+model)
            knifeworks_price_at_zippoby.append(model_data['knifeworksby_price'])
            zippoby_price.append(model_data['zippoby_price'])

        if 'padarunki_price' in model_data:
            padarunki_models.append('z'+model)
            knifeworks_price_at_padarunki.append(model_data['knifeworksby_price'])
            padarunki_price.append(model_data['padarunki_price'])


    trace_1 = go.Bar(
            x=zippoby_models,
            y=knifeworks_price_at_zippoby,
            name='knifeworks.by',
        )
    trace_2 = go.Bar(
            x=zippoby_models,
            y=zippoby_price,
            name='zippo.by',
        )

    data = [trace_1, trace_2]
    layout = go.Layout(
        autosize=False,
        barmode='group'
        )
    fig = go.Figure(data=data, layout=layout)
    url = py.plot(fig, filename='zippo/zippoby-grouped-bar', auto_open=False)
    zippoby_embed = url.replace('https:', '') + '.embed'

    trace_1 = go.Bar(
            x=padarunki_models,
            y=knifeworks_price_at_padarunki,
            name='knifeworks.by',
        )
    trace_2 = go.Bar(
            x=padarunki_models,
            y=padarunki_price,
            name='padarunki.by',
        )

    data = [trace_1, trace_2]
    layout = go.Layout(
        autosize=False,
        barmode='group'
        )
    fig = go.Figure(data=data, layout=layout)
    url = py.plot(fig, filename = 'zippo/padarunki-grouped-bar', auto_open=False)
    padarunki_embed = url.replace('https:', '') + '.embed'

    embed = {"padarunki_embed": padarunki_embed, "zippoby_embed": zippoby_embed}
    return embed
