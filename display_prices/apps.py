from django.apps import AppConfig


class DisplayPricesConfig(AppConfig):
    name = 'display_prices'
