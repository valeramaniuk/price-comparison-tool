from django.db import models


class ZippoModel(models.Model):
    model = models.CharField(max_length=200, primary_key=True)
    description = models.CharField(max_length=400)
    pic_url = models.CharField(max_length=400)
    s3_pic_url = models.CharField(max_length=100, default='')


class Store(models.Model):
    store_name = models.CharField(max_length=100, primary_key=True)

    @classmethod
    def get_all_stores(cls):
        return cls.objects.all().values_list('store_name', flat=True)

    @classmethod
    def get_all_other_stores(cls):
        # except knifeworksby, because we might need to compare with it
        return cls.objects.all().exclude(store_name='knifeworksby').values_list('store_name', flat=True)

    @staticmethod
    def total_lighter_this_store(store_name):
        last_update = Stock.last_update()
        total = Stock.objects.filter(store=store_name, datestamp=last_update) \
            .exclude(price=0) \
            .exclude(price__isnull=True) \
            .count()
        return total


class Stock(models.Model):
    datestamp = models.DateField()
    zippo_model = models.ForeignKey(ZippoModel)
    store = models.ForeignKey(Store, related_name="stock")
    price = models.FloatField()
    model_url = models.CharField(max_length=400)

    class Meta:
        unique_together = ('datestamp', 'zippo_model', 'store')

    @classmethod
    def last_update(cls):
        return cls.objects.all().aggregate(models.Max('datestamp'))['datestamp__max'] \
            .strftime('%Y-%m-%d')
