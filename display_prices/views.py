import logging
import os
from collections import OrderedDict
from django.http import HttpResponse
from django.shortcuts import render
from .models import Stock, Store
from .helpers import price_delta, total_inventory, context_generator
from .graphs.total_stock_chart import get_total_stock_chart

dir_path = os.path.dirname(os.path.realpath(__file__))

logger = logging.getLogger(__name__)
logger.setLevel(logging.INFO)
formatter = logging.Formatter('%(asctime)s:%(name):%(message)s')
file_handler = logging.FileHandler(os.path.join(dir_path, 'views.log'))
file_handler.setFormatter(formatter)
logging_streamer = logging.StreamHandler()
logger.addHandler(file_handler)
logger.addHandler(logging_streamer)


def show_graphs(request):
    embed = redraw_graphs(request)
    print(embed)
    return render(request,
                  'display_prices/graphs.html',
                  {
                      'embed': embed
                  }
                  )


def redraw_graphs(request):
    # it's an ugly construction
    # but to fix it, i need to rewrite the routine for generating 'context'
    # dictionary on SQL
    # TODO: rewrite graphs generation and decouple it from Django project
    # refreshes price comparison charts
    # does not however tells if it was successful or not

    datestamp = Stock.last_update()
    context = context_generator.generate_context(datestamp)

    try:
        embed = get_total_stock_chart(context)
    except:
        return HttpResponse ('Charts cannot be generated')
    return embed

def display_rds_prices(request):
    # Get the date of the last update of the stock table
    # may be partial if called in the middle of the update
    # is a string YYYY-MM-DD
    datestamp = Stock.last_update()

    # for debug during an active update
    # datestamp = '2017-03-20'


    # A list of all stores we have in the DB
    list_of_stores = Store.get_all_stores()

    context = context_generator.generate_context(datestamp=datestamp)

    # get a dictionary with aggregate data for all stores
    total_inventory_obj = total_inventory.get_total_count_object()

    # adds metrics to individual prices
    context = price_delta.add_price_statistics(context, list_of_stores)

    # adds aggregate data for cheaper lighters for all stores
    total_inventory_obj = total_inventory.relative_prices(context=context,
                                                          total_inventory_obj=total_inventory_obj)



    return render(request, 'display_prices/rds_index.html',
                  {
                    'context': context,
                    'total_inventory': total_inventory_obj,
                    'datestamp': datestamp,
                  }
                  )
