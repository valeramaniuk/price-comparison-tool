from django.conf.urls import url
from . import views

urlpatterns = [
    url(r'^$', views.display_rds_prices, name='index'),
    url(r'^graph/$', views.show_graphs, name='graph'),
    url(r'^redraw/$', views.redraw_graphs, name='redraw_graph'),

    ]
